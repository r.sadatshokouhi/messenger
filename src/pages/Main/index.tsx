import { EventType, useInit, useLocale } from "@pouyan-web/core";
import { SlideContainer } from "@pouyan-web/core";
import ChatList from "../../sections/ChatList/ChatList";
import "./Main.scss";

const Main = () => {
  const { setLocaleKey } = useLocale();

  useInit(() => {
    setTimeout(() => {
      setLocaleKey("en-US");
    }, 4000);
  });

  return (
    <div className="sidebar-content">
      <div className="sidebar-header">
        <div className="input-search">
          <input
            className="input-field-input input-search-input"
            type="search"
          />
        </div>
      </div>
      <div className="sidebar-content">
        <SlideContainer
          config={{
            event: EventType.HorizontalSwipe,
            components: [
              { title: "All Chats", component: ChatList },
              { title: "Channels", component: ChatList },
              { title: "Personal", component: ChatList },
              { title: "Music", component: ChatList },
              { title: "Bots", component: ChatList },
              { title: "Archive", component: ChatList },
            ],
          }}
        />
      </div>
    </div>
  );
};

export default Main;
