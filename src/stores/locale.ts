import type { LocaleKeyTypes } from "../@types/locale";

type Locale = Record<LocaleKeyTypes, string>;

export async function getLocales(lang_code: string): Promise<Locale> {
  switch (lang_code) {
    case "fa-IR":
      return {
        Sunday: "یکشنبه",
        Monday: "دوشنبه",
        Tuesday: "سه‌شنبه",
        Wednesday: "چهارشنبه",
        Thursday: "پنج‌شنبه",
        Friday: "جمعه",
        Saturday: "شنبه",
      };
    case "en-US":
    default:
      return {
        Sunday: "Sunday",
        Monday: "Monday",
        Tuesday: "Tuesday",
        Wednesday: "Wednesday",
        Thursday: "Thursday",
        Friday: "Friday",
        Saturday: "Saturday",
      };
  }
}
