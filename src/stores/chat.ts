import { ObjectObservable } from "@pouyan-web/core";
import { ChatEventEnum, ChatType } from "../@types/chatTypes";
import fakeData from "./../../chats.json";

const chatList = fakeData.chats as ChatType[];

export const chatObservable = new ObjectObservable<ChatType>(
  (chat) => chat.object_guid,
);

setTimeout(
  () =>
    handleUpdateChat({
      object_guid: "u0EeOTU084f395b9cffe62fb91805e0e",
      count_unseen: 2,
    }),
  3000,
);
setTimeout(
  () =>
    handleUpdateChat({
      object_guid: "u0EeOTU084f395b9cffe62fb91805e0e",
      last_message: { text: "dlkfjldfj", message_id: "dsfrwet3", type: "Text" },
    }),
  5000,
);
setTimeout(
  () =>
    handleUpdateChat({
      object_guid: "u0EeOTU084f395b9cffe62fb91805e0e",
      is_mute: true,
    }),
  7000,
);
setTimeout(
  () =>
    handleChatEvent(
      {
        object_guid: "u0EeOTU084f395b9cffe62fb91805e0e",
      },
      ChatEventEnum.IsPending,
    ),
  10000,
);
setTimeout(
  () =>
    handleChatEvent(
      {
        object_guid: "g0DduCQ0ddaaaeda8a72282edceed1eb",
      },
      ChatEventEnum.IsFailed,
    ),
  5000,
);
setTimeout(
  () =>
    handleChatEvent(
      {
        object_guid: "u0Cg8AJ0fd0c2ac06f6f18ea785d15dd",
      },
      ChatEventEnum.IsTyping,
    ),
  7000,
);
setTimeout(
  () =>
    handleChatEvent(
      {
        object_guid: "u0KhNU0c5b7d996ac52a04dd7bb68cab",
      },
      ChatEventEnum.IsSelecting,
    ),
  3000,
);

const handleUpdateChat = (newChat: ChatType) => {
  const foundIndex = chatList.findIndex(
    (chat) => chat.object_guid === newChat.object_guid,
  );
  if (foundIndex > -1) {
    const updatedChat = { ...chatList[foundIndex], ...newChat };

    chatList[foundIndex] = updatedChat;
    chatObservable.update(updatedChat);
  }
};

const handleChatEvent = (newChat: ChatType, event: ChatEventEnum) => {
  const foundIndex = chatList.findIndex(
    (chat) => chat.object_guid === newChat.object_guid,
  );
  if (foundIndex > -1) {
    const updatedChat = { ...chatList[foundIndex], ...newChat };

    chatList[foundIndex] = updatedChat;
    chatObservable.eventEmit(chatList[foundIndex], event);
  }
};

export const getChats = () => {
  return chatList;
};
