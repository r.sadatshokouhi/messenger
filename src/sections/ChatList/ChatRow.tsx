import { ListChildComponentProps } from "react-window";
import { ChatType } from "../../@types/chatTypes";
import ChatItem from "./ChatItem";
import Loading from "./Loading";

const ChatRow = ({
  data,
  index,
  style,
}: ListChildComponentProps<{
  chats: ChatType[];
  isItemLoaded: (index: number) => boolean;
}>) => {
  const { chats, isItemLoaded } = data;
  const chat = chats[index];
  if (!isItemLoaded(index) || !chat) {
    return <Loading />;
  }
  return <ChatItem style={style} chat={chat} />;
};

export default ChatRow;
