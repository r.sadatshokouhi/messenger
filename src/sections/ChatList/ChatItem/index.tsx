import { CSSProperties, useRef } from "react";
import { ChatType } from "../../../@types/chatTypes";
import Avatar from "./Avatar";
import Detail from "./Detail";
import useChatClass from "../../../hooks/useChatClass";

const ChatItem = ({
  chat,
  style,
}: {
  chat: ChatType;
  style: CSSProperties;
}) => {
  const chatItemRef = useRef(null);

  useChatClass(chatItemRef, chat);

  return (
    <div style={style} ref={chatItemRef} className="chat-item">
      <Avatar />
      <Detail chat={chat} />
    </div>
  );
};
export default ChatItem;
