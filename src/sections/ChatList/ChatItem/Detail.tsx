import { useState } from "react";
import {
  ChatAbsObjectTypeEnum,
  ChatEventEnum,
  ChatType,
} from "../../../@types/chatTypes";
import { useChatEventObserver } from "../../../hooks/useChatObserver";
import { useDateFormat } from "../../../hooks/useDateFormat";

const Detail = ({ chat }: { chat: ChatType }) => {
  const abs = chat.abs_object;
  const [isTyping, setIsTyping] = useState(false);
  const date = chat.time && useDateFormat(chat.time);

  const _chat = useChatEventObserver(
    chat,
    () => { },
    (event) => {
      if (event === ChatEventEnum.IsTyping) {
        setIsTyping(true);
        setTimeout(() => {
          setIsTyping(false);
        }, 3000);
      }
    },
  );

  return (
    <div className="user-caption">
      <div className="dialog-list dialog-title">
        <span className="user-title">
          {abs?.type === ChatAbsObjectTypeEnum.User
            ? abs.first_name + " " + abs.last_name
            : abs?.title}
        </span>
        <span className="dialog-title-details">
          <span className="message-status">
            <span className="sending-status-icon rbico"></span>
          </span>
          <span className="message-time">{chat.time && date}</span>
        </span>
      </div>

      <div className="dialog-list dialog-subtitle">
        <span className="user-last-message">
          {
            isTyping ?
              <span className="peer-typing-container">
                <span className="peer-typing-description">
                  {abs?.type === ChatAbsObjectTypeEnum.User
                    ? abs.first_name + " " + abs.last_name
                    : abs?.title}
                </span>
                <span className="peer-typing">
                  <span className="peer-typing-dot"></span>
                  <span className="peer-typing-dot"></span>
                  <span className="peer-typing-dot"></span>
                </span>
              </span>
              :
              _chat.last_message?.text
          }
        </span>
        <div className="dialog-subtitle-badge rbico">
          {_chat.count_unseen ? (
            _chat.count_unseen
          ) : null}
        </div>
      </div >
    </div >
  );
};

export default Detail;
