import { useMemo } from "react";
import { getRandomHSL } from "../../../utils/randomColor";

const Avatar = () => {
  const backgroundColor = useMemo(getRandomHSL, []);
  return (
    <div className="avatar-element dialog-avatar" style={{ backgroundColor }}>
    </div>
  );
};

export default Avatar;
