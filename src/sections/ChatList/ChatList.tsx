import { getChats } from "../../stores/chat";
import InfiniteLoader from "react-window-infinite-loader";
import { FixedSizeList as List } from "react-window";
import { useRef, useState } from "react";
import { ChatType } from "../../@types/chatTypes";
import ChatRow from "./ChatRow";

const ChatList = () => {
  const loadedItemsRef = useRef<number[]>([]);
  const [chats, setChats] = useState<ChatType[]>([]);

  const isItemLoaded = (index: number) => {
    return loadedItemsRef.current.includes(index);
  };

  const loadMoreItems = (
    startIndex: number,
    stopIndex: number,
  ): Promise<void> => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          const newItems = getChats().slice(startIndex, stopIndex + 1);
          setChats([...chats, ...newItems]);
          loadedItemsRef.current = [
            ...loadedItemsRef.current,
            ...newItems.map((_, index) => startIndex + index),
          ];
          console.log(newItems);
          resolve();
        } catch (error) {
          reject(error);
        }
      }, 1000); // Simulate a 1-second delay
    });
  };

  const itemData = {
    chats,
    isItemLoaded,
  };

  return (
    <>
      <InfiniteLoader
        isItemLoaded={isItemLoaded}
        itemCount={197}
        loadMoreItems={loadMoreItems}
      >
        {({ onItemsRendered, ref }) => (
          <List
            className="chat-list"
            height={window.innerHeight}
            itemCount={197}
            itemSize={75}
            onItemsRendered={onItemsRendered}
            ref={ref}
            itemData={itemData}
            width={window.innerWidth}
          >
            {ChatRow}
          </List>
        )}
      </InfiniteLoader>
    </>
  );
};

export default ChatList;
