import { PastTimeType, PastTimeTypeEnum } from "../@types/dateTypes";
import withCache from "./withCache";

const WEEKDAYS_FULL = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];

export function getDayStart(datetime: number | Date) {
  const date = new Date(datetime);
  date.setHours(0, 0, 0, 0);
  return date;
}

// @optimization `toLocaleTimeString` is avoided because of bad performance
export function formatTime(datetime: number | Date) {
  const date = typeof datetime === "number" ? new Date(datetime) : datetime;
  const timeFormat = "24h";

  let hours = date.getHours();
  let marker = "";
  // if (timeFormat === "12h") {
  //   marker = hours >= 12 ? " PM" : " AM";
  //   hours = hours > 12 ? hours % 12 : hours;
  // }

  return `${String(hours).padStart(2, "0")}:${String(
    date.getMinutes(),
  ).padStart(2, "0")}${marker}`;
}

export function getDayStartAt(datetime: number | Date) {
  return getDayStart(datetime).getTime();
}

const formatDayToStringWithCache = withCache(
  (
    dayStartAt: number,
    locale: string,
    noYear?: boolean,
    monthFormat: "short" | "long" | "numeric" = "short",
    noDay?: boolean,
  ) => {
    return new Date(dayStartAt).toLocaleString(locale, {
      year: noYear ? undefined : "numeric",
      month: monthFormat,
      day: noDay ? undefined : "numeric",
    });
  },
);

export function formatDateToString(
  datetime: Date | number,
  locale = "en-US",
  noYear = false,
  monthFormat: "short" | "long" | "numeric" = "short",
  noDay = false,
) {
  const date = typeof datetime === "number" ? new Date(datetime) : datetime;
  const dayStartAt = getDayStartAt(date);

  return formatDayToStringWithCache(
    dayStartAt,
    locale,
    noYear,
    monthFormat,
    noDay,
  );
}

export function formatPastTimeShort(datetime: number | Date) {
  const date =
    typeof datetime === "number" ? new Date(datetime * 1000) : datetime;

  const today = getDayStart(new Date());
  if (date >= today) {
    return formatTime(date);
  }

  const weekAgo = new Date(today);
  weekAgo.setDate(today.getDate() - 7);
  if (date >= weekAgo) {
    return WEEKDAYS_FULL[date.getDay()];
  }

  const noYear = date.getFullYear() === today.getFullYear();

  return formatDateToString(
    date,
    "en-US",
    noYear,
    noYear ? "short" : "numeric",
  );
}

export const getPastTime = (datetime: number | Date): PastTimeType => {
  const date =
    typeof datetime === "number" ? new Date(datetime * 1000) : datetime;

  const today = getDayStart(new Date());
  if (date >= today) {
    return { time: formatTime(date), type: PastTimeTypeEnum.Time };
  }

  const weekAgo = new Date(today);
  weekAgo.setDate(today.getDate() - 7);
  if (date >= weekAgo) {
    return { weekday: date.getDay(), type: PastTimeTypeEnum.Weekday };
  }

  const noYear = date.getFullYear() === today.getFullYear();

  if (noYear) {
    return {
      type: PastTimeTypeEnum.Month_Day,
      day: date.getDay(),
      month: date.getMonth(),
    };
  } else
    return {
      type: PastTimeTypeEnum.Full_Date,
      day: date.getDay(),
      month: date.getMonth(),
      year: date.getFullYear(),
    };
};

export const formatPastTime = (date: number | Date): PastTimeType => {
  const dateWithType = getPastTime(date);
  switch (dateWithType.type) {
    case PastTimeTypeEnum.Time:
      return dateWithType;
    case PastTimeTypeEnum.Weekday:
      return {
        weekday: WEEKDAYS_FULL[dateWithType.weekday as number],
        type: PastTimeTypeEnum.Weekday,
      };
    case PastTimeTypeEnum.Month_Day:
    case PastTimeTypeEnum.Full_Date:
      return dateWithType;
    default:
      return dateWithType;
  }
};
