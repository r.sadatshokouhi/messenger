export const getRandomHSL = () => {
  const h = Math.floor(Math.random() * (240 - 120 + 1)) + 120;
  const s = Math.random() * (80 - 40) + 40;
  const l = Math.random() * (70 - 40) + 40;
  return `hsl(${h}, ${s}%, ${l}%)`;
};
