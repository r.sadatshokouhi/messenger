const locale = {
  weekdaysFull: {
    Sunday: "یکشنبه",
    Monday: "دوشنبه",
    Tuesday: "سه‌شنبه",
    Wednesday: "چهارشنبه",
    Thursday: "پنج‌شنبه",
    Friday: "جمعه",
    Saturday: "شنبه",
  },
};
export type LocaleRootPropes = keyof typeof locale;
export type LocaleKeyProp<T extends LocaleRootPropes> =
  keyof (typeof locale)[T];

export type LocaleKeyWeekdays = LocaleKeyProp<"weekdaysFull">;
export type LocaleKeyTypes = LocaleKeyWeekdays;
