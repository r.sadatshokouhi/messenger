interface Message {
  message_id: string;
  type: "Text" | "Image" | "Video" | "Audio" | "File" | "Other"; // Expand for potential message types
  text?: string; // Optional for text messages
  author_object_guid?: string;
  is_mine?: boolean;
  author_type?: "User" | "Group" | "Channel" | "Bot"; // Expand for potential author types
}

export enum ChatAbsObjectTypeEnum {
  User = "User",
  Group = "Group",
  Channel = "Channel",
  Bot = "Bot",
}

interface ChatAbsObject {
  object_guid: string;
  type: ChatAbsObjectTypeEnum;
  first_name?: string; // Optional for users
  last_name?: string; // Optional for users
  avatar_thumbnail?: AvatarThumbnail; // Optional for users
  is_verified?: boolean; // Optional for users
  is_deleted?: boolean; // Optional for users
  user_guid?: string; // Optional for users
  title?: string;
}

interface AvatarThumbnail {
  file_id: string;
  mime: string;
  dc_id: string;
  access_hash_rec: string;
  size: number;
}

export interface ChatType {
  object_guid: string;
  access?: string[];
  count_unseen?: number;
  is_mute?: boolean;
  is_pinned?: boolean;
  time_string?: string;
  last_message?: Message;
  last_seen_my_mid?: string;
  last_seen_peer_mid?: string;
  status?: "Active" | "Inactive";
  time?: number;
  abs_object?: ChatAbsObject;
  is_blocked?: boolean;
  last_message_id?: string;
  last_deleted_mid?: string;
  is_in_contact?: boolean;
  show_ask_spam?: boolean;
}

export enum ChatEventEnum {
  IsPending = "IsPending",
  IsFailed = "IsFailed",
  IsTyping = "IsTyping",
  IsSending = "IsSending",
  IsSelecting = "IsSelecting",
}
