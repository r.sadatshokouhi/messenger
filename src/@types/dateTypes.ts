export enum PastTimeTypeEnum {
  Time = "Time",
  Weekday = "Weekday",
  Month_Day = "Month_Day",
  Full_Date = "Full_Date",
}

export type PastTimeType =
  | {
      type: PastTimeTypeEnum.Time;
      time: string;
    }
  | {
      type: PastTimeTypeEnum.Weekday;
      weekday: number | string;
    }
  | {
      type: PastTimeTypeEnum.Month_Day;
      month: number;
      day: number;
    }
  | {
      type: PastTimeTypeEnum.Full_Date;
      month: number;
      day: number;
      year: number;
    };
