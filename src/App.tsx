import { useEffect } from "react";
import { LocaleContextProvider, MasterTabContainer } from "@pouyan-web/core";
import { openView } from "@pouyan-web/core";
import Main from "./pages/Main";
import { getLocales } from "./stores/locale";

function App() {
  useEffect(() => {
    openView({
      id: "d",
      component: Main,
      type: "MasterTab",
    });
  }, []);

  return (
    <LocaleContextProvider defaultLocaleKey="fa-IR" getAsyncLocale={getLocales}>
      <div className="whole">
        <div id="main-columns" className="tabs-container">
          <div id="column-left" className="main-column">
            <MasterTabContainer />
          </div>
        </div>
      </div>
    </LocaleContextProvider>
  );
}

export default App;
