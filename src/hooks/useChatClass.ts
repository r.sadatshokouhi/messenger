import { MutableRefObject, useEffect } from "react";
import { useChatEventObserver } from "./useChatObserver";
import { ChatEventEnum, ChatType } from "../@types/chatTypes";

const useChatClass = (ref: MutableRefObject<any>, chat: ChatType) => {
  const setChatState = (chat: ChatType) => {
    toggleClass("is-muted", chat.is_mute);
    toggleClass("is-pinned", chat.is_pinned);
    toggleClass("is-unseen", (chat.count_unseen || 0) > 0);
    toggleClass("is-sent", chat.last_message?.is_mine);
    toggleClass(
      "is-seen",
      !chat.last_message?.is_mine &&
        chat.last_message?.message_id === chat.last_seen_peer_mid,
    );
    toggleClass("is-verified", chat.abs_object?.is_verified);
  };

  const handleChatEvent = (event: ChatEventEnum) => {
    toggleClass("is-pending", event === ChatEventEnum.IsPending);
    toggleClass("is-failed", event === ChatEventEnum.IsFailed);
    toggleClass("is-typing", event === ChatEventEnum.IsTyping);
    toggleClass("is-sending", event === ChatEventEnum.IsSending);
    toggleClass("is-selecting", event === ChatEventEnum.IsSelecting);
  };

  const _chat = useChatEventObserver(chat, setChatState, handleChatEvent);

  const toggleClass = (cssClass: string, hasClass?: boolean) => {
    const classList = (ref.current as HTMLElement)?.classList;
    classList.toggle(cssClass, hasClass);
  };

  useEffect(() => {
    setChatState(_chat);
  }, []);
};
export default useChatClass;
