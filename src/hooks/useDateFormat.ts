import { useLocale } from "@pouyan-web/core";
import { PastTimeTypeEnum } from "../@types/dateTypes";
import { formatPastTime } from "../utils/dateFormat";
import withCache from "../utils/withCache";

export const useDateFormat = (date: number | Date) => {
  const formattedDate = formatPastTime(date);
  const { lang, getLocaleKey } = useLocale();
  const localeKey = getLocaleKey();
  switch (formattedDate?.type) {
    case PastTimeTypeEnum.Time:
      return formattedDate.time;
    case PastTimeTypeEnum.Weekday:
      return lang(formattedDate.weekday as any);
    case PastTimeTypeEnum.Month_Day:
      return withCache(() =>
        new Date(0, formattedDate.month, formattedDate.day).toLocaleString(
          localeKey,
          {
            year: undefined,
            month: "short",
            day: "numeric",
          },
        ),
      )();
    case PastTimeTypeEnum.Full_Date:
      return withCache(() =>
        new Date(
          formattedDate.year,
          formattedDate.month,
          formattedDate.day,
        ).toLocaleString(localeKey, {
          year: "numeric",
          month: "numeric",
          day: "numeric",
        }),
      )();
    default:
      return "";
  }
};
