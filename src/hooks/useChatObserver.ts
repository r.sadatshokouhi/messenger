import { useEventObserver } from "@pouyan-web/core";
import { ChatEventEnum, ChatType } from "../@types/chatTypes";
import { chatObservable } from "../stores/chat";

export const useChatEventObserver = (
  chat: ChatType,
  update?: (chat: ChatType) => void,
  on?: (action: ChatEventEnum) => void,
) =>
  useEventObserver<ChatType, ChatEventEnum>(
    chatObservable,
    chat,
    () => {},
    update,
    on,
  );
