import random
import json

avatar_types = ["SAVE_MESSAGE", "PICTURE", "ABS_NAME", "COLOR"]

chat_data = []

for i in range(20):
  chat = {}
  chat["chat_name"] = f"Chat {random.randint(1, 100)}"
  chat["unread_message_count"] = random.randint(0, 10)
  chat["mute"] = bool(random.randint(0, 1))
  chat["last_message"] = {}
  chat["last_message"]["id"] = id,
  chat["last_message"]["sender"] = random.choice(["Alice", "Bob", "Charlie", "Diana", "Eve", "مامان", "ریحانه"])
  chat["last_message"]["message"] = f"This is a message from {chat['last_message']['sender']}"
  chat["last_message"]["file_type"] = random.choice(["none", "image", "document"])
  chat["last_message"]["typing"] = bool(random.randint(0, 1))
  chat["last_message"]["sending_file"] = bool(random.randint(0, 1))
  chat["message_status"] = random.choice(["sending", "draft", "failed", "sent", "seen"])
  chat["last_message_time"] = f"2024-01-24T{random.randint(10, 23)}:{random.randint(0, 59)}:{random.randint(0, 59)}Z"
  chat["pin"] = bool(random.randint(0, 1))
  chat["is_verified"] = bool(random.randint(0, 1))
  chat["avatar"] = random.choice(avatar_types)
  chat_data.append(chat)

with open("fakeData.json", "w") as f:
  json.dump(chat_data, f, indent=2)

print("20 chat objects generated and saved to chat_data.json")